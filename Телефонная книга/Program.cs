﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Телефонная_книга
{
    //проверка первого коммита
    class Program
    {

        public Dictionary<string, NumbersData> people = new Dictionary<string, NumbersData>();
        public int count = 0;
        static void Main(string[] args)
        {
            Program p = new Program();
            string quit;            
            while (true)
            {
                Console.WriteLine("Добрый день! Чтобы вы хотели сделать в телефонной книге?\n(выберите соответствующую цифру) \n    1.Создать новую запись... \n    2.Редактировать запись... \n    3.Удалить запись... \n    4.Просмотр конкретной записи... \n    5.Просмотр всех записей в упрощенной форме...");
                int firstCom = Int32.Parse(Console.ReadLine());
                switch (firstCom)
                {
                    case 1:
                        p.AddHuman();
                        break;
                    case 2:
                        Console.WriteLine("Введите фамилию и имя через пробел (например:Иванов Иван)");
                        p.people[Console.ReadLine()].EditData();
                        break;
                    case 3:
                        Console.WriteLine("Введите фамилию и имя через пробел (например:Иванов Иван)");
                        p.people[Console.ReadLine()].DeleteData();
                        break;
                    case 4:
                        Console.WriteLine("Введите фамилию и имя через пробел (например:Иванов Иван)");
                        p.people[Console.ReadLine()].ShowOneData();
                        break;
                    case 5:
                        p.ShowAllData();
                        break;
                    case 6:
                        //для тестов.. выходит из switch-case
                        break;
                }                
                Console.WriteLine("Выйти?");
                quit = Console.ReadLine();
                if (quit=="да")
                {
                    break;
                }    
            }

        }
        //добавляет новую запись
        public void AddHuman()
        {
            Console.WriteLine("Поля помеченные * обязательны! Если вы не хотите вводить информацию в необязательное поле, введите: no ");
            Console.Write("   Фамилия*: ");
            string lName = Console.ReadLine();
            Console.Write("   Имя*: ");
            string fName = Console.ReadLine();
            Console.Write("   Отчество: ");
            string tName = Console.ReadLine();
            Console.Write("   Номер*: ");
            string number = Console.ReadLine();
            Console.Write("   Страна*: ");
            string country = Console.ReadLine();
            Console.WriteLine("Дата рождения вводится через точку (например 10.10.2010)");
            Console.Write("   Дата рождения: ");
            string birthD = Console.ReadLine();
            Console.Write("   Организация: ");
            string company = Console.ReadLine();
            Console.Write("   Должность: ");
            string specialization = Console.ReadLine();
            Console.Write("   Прочие заметки:");
            string notes = Console.ReadLine();
            people.Add(lName + " " + fName, new NumbersData(fName, lName, number, country));
            people[lName + " " + fName].thirdName = tName;
            people[lName + " " + fName].birthDay = birthD;
            people[lName + " " + fName].company = company;
            people[lName + " " + fName].specialization = specialization;
            people[lName + " " + fName].notes = notes;
            count++;
        }

        //показать данные всех в упрощенном виде (фамилия, имя, номер)
        public void ShowAllData()
        {
            foreach (NumbersData t in people.Values)
            {
                Console.WriteLine($"Фамилия: {t.lastName}\nИмя: {t.firstName}\nНомер: {t.number}\n__________________________________________________________");
            }
        }
    }



    public class NumbersData
    {
        //firstName - имя, lastName - фамилия, thirdName - отчество
        public string firstName, lastName, thirdName;
        //номер телефона
        public string number;
        //страна
        public string country;
        //дата рождения
        public string birthDay;
        //место работы, должность
        public string company, specialization;
        //заметки
        public string notes;

        public NumbersData(string firstName, string lastName, string number, string country)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.number = number;
            this.country = country;
        }

        //редактирование данных
        public void EditData()
        {
            Console.Write("Какой пункт вы хотите изменить?");
            string id = Console.ReadLine();
            Console.Write("Введите новое значение:");
            string newInfo = Console.ReadLine();
            switch (id)
            {
                case "a":
                    this.lastName = newInfo;
                    break;

                case "b":
                    this.firstName = newInfo;
                    break;

                case "c":
                    this.thirdName = newInfo;
                    break;

                case "d":
                    if (Int32.TryParse(newInfo, out int a))
                    {
                        this.number = newInfo;
                    }                    
                    break;

                case "e":
                    this.country = newInfo;
                    break;

                case "f":
                    this.birthDay = newInfo;
                    break;

                case "g":
                    this.company = newInfo;
                    break;

                case "h":
                    this.specialization = newInfo;
                    break;

                case "i":
                    this.notes = newInfo;
                    break;

            }
        }

        //удаление данных
        public void DeleteData()
        {
            this.firstName = "being removed";            
        }


        //показать данные об одном человеке
        public void ShowOneData()
        {
            if (this.firstName == "being removed")
            {
                Console.WriteLine("Запись была удалена...");
            }
            else
            {
                Console.WriteLine($"Имя: {this.firstName} \nФамилия: {this.lastName} \nОтчество: {this.thirdName} \nНомер: {this.number} \nСтрана: {this.country} \nДата рождения: {this.birthDay} \nОрганизация: {this.company} \nДолжность: {this.specialization} \nПрочие заметки: {this.notes}");
            }
        }        
    }
}
